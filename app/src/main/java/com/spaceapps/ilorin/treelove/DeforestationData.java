package com.spaceapps.ilorin.treelove;

import java.util.Date;

/**
 * Created by victor on 4/17/15.
 */
public class DeforestationData {
    Location location;
    Double landarea;
    String reason;
    String picture;
    Date date;
    User user;


    public DeforestationData(Location location, Double landarea, String reason, String picture, Date date, User user) {
        this.location = location;
        this.landarea = landarea;
        this.reason = reason;
        this.picture = picture;
        this.date = date;
        this.user = user;
    }
}




