package com.spaceapps.ilorin.treelove;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.liveo.interfaces.NavigationLiveoListener;
import br.liveo.navigationliveo.NavigationLiveo;


public class HomeActivity extends NavigationLiveo implements NavigationLiveoListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onUserInformation() {
        //User information here
        this.mUserName.setText("Rudson Lima");
        this.mUserEmail.setText("rudsonlive@gmail.com");
        this.mUserPhoto.setImageResource(R.drawable.ic_rudsonlive);
        this.mUserBackground.setImageResource(R.drawable.ic_launcher);
    }
    @Override
    public void onInt(Bundle savedInstanceState) {
        //Creation of the list items is here

        // set listener {required}
        this.setNavigationListener(this);

        // name of the list items
        List<String> mListNameItem = new ArrayList<>(4);
        mListNameItem.add(0, getString(R.string.home));
        mListNameItem.add(1, getString(R.string.volunteer));
        mListNameItem.add(2, getString(R.string.plantatree));
        mListNameItem.add(3, getString(R.string.profile));


        // icons list items
        List<Integer> mListIconItem = new ArrayList<>();

        mListIconItem.add(0, 0); //Item no icon set 0
        mListIconItem.add(1, 0);
        mListIconItem.add(2, 0);
        mListIconItem.add(3, 0); //Item no icon set 0


//        //{optional} - Among the names there is some subheader, you must indicate it here
//        List<Integer> mListHeaderItem = new ArrayList<>();
//        mListHeaderItem.add(4);

        //{optional} - Among the names there is any item counter, you must indicate it (position) and the value here
//        SparseIntArray mSparseCounterItem = new SparseIntArray(); //indicate all items that have a counter
//        mSparseCounterItem.put(0, 7);
//        mSparseCounterItem.put(6, 250);

//        //If not please use the FooterDrawer use the setFooterVisible(boolean visible) method with value false
//        this.setFooterInformationDrawer(R.string.settings, R.drawable.ic_settings_black_24dp);

        this.setNavigationAdapter(mListNameItem, mListIconItem, null, null);
    }


    @Override
    public void onClickFooterItemNavigation(View view) {

    }

    @Override
    public void onClickUserPhotoNavigation(View view) {

    }

    @Override //The "layoutContainerId" should be used in "beginTransaction (). Replace"
    public void onItemClickNavigation(int position, int layoutContainerId) {

        FragmentManager mFragmentManager = getSupportFragmentManager();
        Fragment mFragment = new DeforestationList().newInstance();

        if (mFragment != null){
            mFragmentManager.beginTransaction().replace(layoutContainerId, mFragment).commit();
        }
    }

    @Override
    public void onPrepareOptionsMenuNavigation(Menu menu, int position, boolean visible) {

        //hide the menu when the navigation is opens
        switch (position) {
            case 0:
//                menu.findItem(R.id.menu_add).setVisible(!visible);
             //   menu.findItem(R.id.menu_search).setVisible(!visible);
                break;

            case 1:
               // menu.findItem(R.id.menu_add).setVisible(!visible);
               // menu.findItem(R.id.menu_search).setVisible(!visible);
                break;
        }
    }
 }
