package com.spaceapps.ilorin.treelove;

public class User
    {
        String name;
        String city;
        String email;
        String username;

        public User(String name, String city, String email, String username) {
            this.name = name;
            this.city = city;
            this.email = email;
            this.username = username;
        }
    }